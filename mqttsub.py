import paho.mqtt.client as mqtt
import sqlite3

conn = sqlite3.connect('twocolumns.db')
curs = conn.cursor()

def on_connect(client, userdata, rc):
	print "Connected with result code " + str(rc)
	client.subscribe("explore")

def on_message(client, userdata, msg):
	print "Topic: ", msg.topic + '\nMessage: ' + str(msg.payload)
	stringg = "INSERT INTO table1 values('%s','%s')" % (msg.payload.split()[0], msg.payload.split()[1])
	curs.execute(stringg)
	conn.commit()

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("sufiankaki.me", 1883, 60)

client.loop_forever()
