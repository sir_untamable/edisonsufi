#!/home/root/todo-api/flask/bin/python
from flask import Flask, jsonify
import sqlite3

tasks = []
app = Flask(__name__)

def updatetsk():
	del tasks[:]
	conn = sqlite3.connect('twocolumns.db')
	curs = conn.cursor()
	for row in curs.execute("SELECT * FROM table1"):
		tasks.append({'item1': row[0], 'item2': row[1]})
	conn.close()
	return tasks

@app.route('/explore', methods = ['GET'])

def get_tasks():
	updatetsk()
	return jsonify({'tasks' : tasks})

if __name__ == '__main__':
	app.run(debug = True, host = '0.0.0.0')

